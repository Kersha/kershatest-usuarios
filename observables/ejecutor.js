const paquetes = require('../promesas/paquetes')
const { Observable, Subject, ReplaySubject, from, of, range, throwError } = require('rxjs');
const { switchMap, map, filter, distinct, mergeMap, catchError } = require('rxjs/operators');

var usuarios = [
    { nombre: 'Carlos'}, { nombre: 'Andres' },
    { nombre: 'Alexia'},
    { nombre: 'Johanna' }, { nombre: 'Ricardo' }
];

const mostrarResultadoExito = (resultadoExito) => {
    console.log(resultadoExito);
};

const mostrarResultadoError = (resultadoError) => {
    console.log(resultadoError,'hubo error');
};

const seTermino = () => {
    console.log('se completo la actividad');
};


const crear$ = from(paquetes.crearUsuario({nombre:'Carlos2'},usuarios))
const buscarTodos$ = from( paquetes.buscarTodasCoincidencias('ar',usuarios));
const buscarUno$ = from(paquetes.buscarUsuario({nombre:'Carlos'},usuarios));
const buscarOCrear$ = from(paquetes.buscarOCrearUsuario({nombre:'Carlos3'},usuarios));
const eliminar$= from(paquetes.eliminarUsuarios({nombre:'Carlos'},usuarios));


eliminar$.subscribe(
    mostrarResultadoExito,
    mostrarResultadoError,
    seTermino
)