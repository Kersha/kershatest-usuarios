module.exports = (usuarioBuscado,usuarios)=>{

    return new Promise((resolve,reject)=>{
        const existeUsuario = usuarios.find((usuario)=>{
            return JSON.stringify(usuario).toUpperCase() === JSON.stringify(usuarioBuscado).toUpperCase()
        } );
        if(existeUsuario){
            resolve({
                mensaje: 'se encontro al usuario',
                usuarioBuscado
            });
        }else{
            reject({
                mensaje: 'usuario no encontrado'
            });
        }
    });
};