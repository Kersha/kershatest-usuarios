var buscarUsuario = require('./buscarUsuario');
var crearUsuario =  require('./crearUsuario');

module.exports = (usuario, usuarios) =>{
 
    return new Promise((resolve, reject)=>{
        buscarUsuario(usuario, usuarios)
        .then((resultado)=>{
            resolve(usuarios);
        })
        .catch((error)=>{
           return crearUsuario(usuario,usuarios)
        })
        .then((resultado)=>{
            resolve(usuarios);
        })

    });

};