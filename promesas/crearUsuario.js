module.exports = (nuevoUsuario,usuarios)=>{
    return new Promise((resolve,reject)=>{
        try {
            usuarios.push(nuevoUsuario);
            resolve({
                mensaje: 'se creo el usuario '+nuevoUsuario,
                usuarios
            });
        } catch (error) {
            reject({
                mensaje: 'error creando usuario',
                error
            })
        }
    })
};