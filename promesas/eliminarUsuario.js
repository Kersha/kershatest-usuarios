const buscarUsuario = require('./buscarUsuario');

module.exports = (usuario,usuarios)=> {

    return new Promise((resolve, reject)=>{
        buscarUsuario(usuario,usuarios)
        .then((resultado)=>{
            const indiceUsuarioEliminar = usuarios.reduce((anterior,actual,indice)=>{
                const soniguales=JSON.stringify(actual)===JSON.stringify(usuario);
                if(soniguales){
                    return anterior+indice;
                }
                return anterior
            },0);
            usuarios.splice(indiceUsuarioEliminar,1);
            resolve(usuarios);
        })
        .catch((resultado)=>{
            reject(resultado);
        })
    });
   
}
