module.exports= (expresion, usuarios)=> {

    return new Promise((resolve,reject)=>{
        const usuariosCoincide = usuarios.filter((usuario)=>{
            return usuario.nombre.toLowerCase().match(new RegExp(""+expresion.toLowerCase()+""))
            //return usuario.nombre.match(expresion) // recibe una regex no da match en diferentes mayusculas o minisculas
        });

        if(usuariosCoincide.length>0){
            resolve({
                    mensaje:'Se encontraron resultados',
                    usuariosCoincide
                });
        }else{
            reject({
                mensaje:'NO se encontraron resultados',
                usuariosCoincide
            });
        }
    })

}