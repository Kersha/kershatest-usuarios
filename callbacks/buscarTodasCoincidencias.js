module.exports= (expresion, usuarios, callback)=> {

    const usuariosCoincide = usuarios.filter((usuario)=>{
        return usuario.nombre.toLowerCase().match(new RegExp(""+expresion.toLowerCase()+""))
        //return usuario.nombre.match(expresion) // recibe una regex no da match en diferentes mayusculas o minisculas
    });

    callback(usuariosCoincide)
}