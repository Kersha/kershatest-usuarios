var buscarUsuario = require('./buscarUsuario');
var crearUsuario =  require('./crearUsuario');

module.exports = (usuario, usuarios,callback) =>{
 
    buscarUsuario(usuario,usuarios,(encontroUsuario)=>{
        if(encontroUsuario){
            console.log("se encontro el usuario: "+ JSON.stringify(encontroUsuario));
            callback(usuarios);
        }else{
            console.log("se encontro el usuario: "+ JSON.stringify(encontroUsuario));
            usuarios = crearUsuario(usuario,usuarios,(nuevoUsuario,nuevosUsuarios)=>{
                console.log('se creo el usuario: '+ JSON.stringify(nuevoUsuario));
                usuarios=nuevosUsuarios;
                callback(usuarios);
            });
        }
    });  
};