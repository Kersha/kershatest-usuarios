const buscarUsuario = require('./buscarUsuario');
const crearUsuario =  require('./crearUsuario');
const buscarOCrearUsuario = require('./buscaroCrearUsuario');
const eliminarUsuarios = require('./eliminarUsuario');
const buscarTodasCoincidencias= require('./buscarTodasCoincidencias');

module.exports = {
    buscarOCrearUsuario,
    buscarUsuario,
    crearUsuario,
    eliminarUsuarios,
    buscarTodasCoincidencias
}
