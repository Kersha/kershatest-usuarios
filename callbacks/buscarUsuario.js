module.exports = (usuarioBuscado,usuarios,callback)=>{
    const existeUsuario = usuarios.find((usuario)=>{
    
        return JSON.stringify(usuario).toUpperCase() === JSON.stringify(usuarioBuscado).toUpperCase()
        
    } );
    callback(existeUsuario);
};