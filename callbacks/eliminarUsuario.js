const buscarUsuario = require('./buscarUsuario');

module.exports = (usuario,usuarios,callback)=> {
    buscarUsuario(usuario,usuarios,(encontroUsuarios)=>{
        if(encontroUsuarios){
            const indiceUsuarioEliminar = usuarios.reduce((anterior,actual,indice)=>{
                const soniguales=JSON.stringify(actual)===JSON.stringify(usuario);
                if(soniguales){
                    return anterior+indice;
                }
                return anterior
            },0);
            usuarios.splice(indiceUsuarioEliminar,1);
            callback(usuarios);
        }else{
            callback(usuarios);
        }
    })
}
