const paquetePromesas = require('./promesas/paquetes.js');
const paqueteCallbacks = require ('./callbacks/paquetes.js');
module.exports.paquete = {
    paqueteCallbacks,
    paquetePromesas
};

module.exports.usuarios = [
    { nombre: 'Carlos'}, { nombre: 'Andres' },
    { nombre: 'Alexia'},
    { nombre: 'Johanna' }, { nombre: 'Ricardo' }
];

module.exports.usuarioBuscadoPositivo = {
    nombre: 'Carlos'
};

module.exports.usuarioBuscadoNegativo = {
    nombre: 'Kersha'
};

module.exports.usuarioCrear = {
    nombre: 'Kersha'
};

module.exports.usuarioEliminarPositivo = {
    nombre:'Carlos'
};

module.exports.usuarioEliminarNegativo = {
    nombre:'Kersha'
};

module.exports.expresionPositiva = 'car';

module.exports.expresionNegativa = 'asqw';


